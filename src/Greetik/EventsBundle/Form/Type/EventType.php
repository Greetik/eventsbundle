<?php

namespace Greetik\EventsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventType
 *
 * @author Paco
 */
class EventType extends AbstractType {

    

    public function buildForm(FormBuilderInterface $builder, array $options) {

        if (count($options['projects'])>0) {
            $builder
                    ->add('project', 'choice', array(
                        'choices' => $options['projects'],
                        'required' => true,
                        'expanded' => false,
                        'multiple' => false
            ));
        }
        
        if (!empty($options['_date'])){
            $datadate = array('data'=>$options['_date']);
        }else $datadate = array();
        
        
        $builder
                ->add('title', TextType::class)
                ->add('color')
                ->add('body', TextareaType::class, array('required' => false))
                ->add('fromdate', DatetimeType::class, array_merge(array('widget' => 'single_text', 'format' => 'dd/MM/yyyy - HH:mm'), $datadate))
                ->add('todate', DatetimeType::class, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy - HH:mm', 'required' => false))
                ->add('metatitle', TextType::class, array('required' => false))
                ->add('metadescription', TextType::class, array('required' => false))
                ->add('extra', TextType::class, array('required' => false));
    }

    public function getName() {
        return 'Event';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\EventsBundle\Entity\Event',
            '_date' => '',
            'projects' => array()
        ));
    }

}
