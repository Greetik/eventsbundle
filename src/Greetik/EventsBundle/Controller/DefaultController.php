<?php

namespace Greetik\EventsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\EventsBundle\Entity\Event;
use Greetik\EventsBundle\Form\Type\EventType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

    /**
     * Render all the events of the user
     * 
     * @author Pacolmg
     */
    public function indexAction(Request $request, $idproject) {

        if ($request->getMethod() == 'POST') {
            try {
                $data = $this->get($this->getParameter('events.permsservice'))->getEvents($idproject, $this->get('beinterface.tools')->getDateFromParams($request->get('start')), $this->get('beinterface.tools')->getDateFromParams($request->get('end')), true);
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
            return new Response(json_encode($data), 200, array('Content-Type' => 'application/json'));
        }

        return $this->render($this->getParameter('events.interface') . ':index.html.twig', array(
                    'data' => $this->get($this->getParameter('events.permsservice'))->getAllEventsByProject($idproject),
                    'idproject' => $idproject,
                    'insertAllow' => true
        ));
    }

    /**
     * View an individual event, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction($id) {
        $event = $this->get($this->getParameter('events.permsservice'))->getEvent($id);
        $editForm = $this->createForm(EventType::class, $this->get('events.tools')->getEventObject($id));
        return $this->render($this->getParameter('events.interface') . ':view.html.twig', array(
                    'option' => '',
                    'item' => $event,
                    'new_form' => $editForm->createView(),
                    'modifyAllow' => $this->get($this->getParameter('events.permsservice'))->getEventPerm($event, 'modify')
        ));
    }

    /**
     * Get the data of a new Event by Event and persis it
     * 
     * @param Event $item is received by Event Request
     * @author Pacolmg
     */
    public function insertAction(Request $request, $idproject, $date='') {
        $event = new Event();

        $newForm = $this->createForm(EventType::class, $event, (empty($date)?array():array('_date'=>$this->get('beinterface.tools')->getDateFromParams($date))));

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                    $this->get($this->getParameter('events.permsservice'))->insertEvent($event, $idproject, $this->getUser()->getId());
                    return $this->redirect($this->generateUrl('events_listevents', array('idproject' => $idproject)));
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            } else {
                $this->addFlash('error', (string) $newForm->getErrors(true, true));
            }
        }

        return $this
                        ->render($this->getParameter('events.interface') . ':insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an event
     * 
     * @param int $id is received by Get Request
     * @param Event $item is received by Event Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {
        $event = $this->get('events.tools')->getEventObject($id);

        $editForm = $this->createForm(EventType::class, $event);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('events.permsservice'))->modifyEvent($event);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirect($this->generateUrl('events_viewevent', array('id' => $id)));
    }

    public function deleteAction(Request $request, $id) {

        $event = $this->get('events.tools')->getEventObject($id);
        if ($request->getMethod() == "POST") {
            $this->get($this->getParameter('events.permsservice'))->deleteEvent($event);
            return $this->redirect($this->generateUrl('events_listevents', array('idproject' => $event->getProject())));
        }
        return $this->redirect($this->generateUrl('events_listevents'));
    }
    
    public function getpositionAction($id){
        try {
            $event = $this->get($this->getParameter('events.permsservice'))->getEvent($id);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }

        return new Response(json_encode(array('errorCode' => 0, 'data' => array('color' => substr($event['color'],1), 'lat' => $event['lat'], 'lon' => $event['lng']))), 200, array('Content-Type' => 'application/json'));         
    }

    public function savepositionAction(Request $request, $id) {
        try {
            $event = $this->get('events.tools')->getEventObject($id);
            $event->setLat($request->get('lat'));
            $event->setLng($request->get('lon'));
            $this->get($this->getParameter('events.permsservice'))->modifyEvent($event);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }

        return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
    }

}
