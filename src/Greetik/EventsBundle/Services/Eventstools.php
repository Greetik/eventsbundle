<?php

namespace Greetik\EventsBundle\Services;

use Greetik\EventsBundle\Entity\Event;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Greetik\EventsBundle\Entity\Tag;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Eventstools {

    private $em;
    private $interfacetools;

    public function __construct($_entityManager, $_interfacetools) {
        $this->em = $_entityManager;
        $this->interfacetools = $_interfacetools;
    }

    public function getEventPerm($perm, $event){
        return true;
    }
            
    public function getAllEventsByProject($project = '') {
        if (empty($project))
            return $this->em->getRepository('EventsBundle:Event')->findAll();
        return $this->em->getRepository('EventsBundle:Event')->findAllByProject($project);
    }

    public function getEvents($project='', $from='', $to=''){
        $data = $this->em->getRepository('EventsBundle:Event')->getEvents($project, $from, $to);
        foreach($data as $k=>$v){
            $data[$k]['start'] = $v['fromdate']->format('Y-m-d H:i');
            if (!$v['todate']){
                $data[$k]['end'] = $v['fromdate']->format('Y-m-d 23:59');
            }
            else{
                $data[$k]['end'] = $v['todate']->format('Y-m-d H:i');
            }
            $data[$k]['type'] = 'event';
        }
        return $data;
    }
    
    public function getNextEvents($project = '', $num=0, $from='') {
        return $this->em->getRepository('EventsBundle:Event')->getNextEvents($project, $num, $from);
    }


    public function getEventsByExtra($text) {
        return $this->em->getRepository('EventsBundle:Event')->getEventByExtra($text);
    }

    public function getEvent($id) {
        $event = $this->em->getRepository('EventsBundle:Event')->getEvent($id);

        if (!$event)
            throw new NotFoundHttpException('No se encuentra el event');

        return $event;
    }

    public function getEventObject($id) {
        $event = $this->em->getRepository('EventsBundle:Event')->findOneById($id);

        if (!$event)
            throw new NotFoundHttpException('No se encuentra el event');

        return $event;
    }

    public function modifyEvent($event) {
        $event->setLastedit(new \DateTime());

        $this->em->flush();
    }

    public function insertEvent($event, $project, $user) {
        $event->setCreatedat(new \DateTime());
        $event->setProject($project);
        $event->setUser($user);

        $this->em->persist($event);
        $this->em->flush();
    }

    public function deleteEvent($event) {
        $this->em->remove($event);
        $this->em->flush();
    }

    public function getNumberOfEventsByProject($project) {
        return $this->em->getRepository('EventsBundle:Event')->findNumberOfEventsByProject($project);
    }


}
